package punktimäng;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Raskem extends JPanel implements ActionListener, KeyListener{
	
//Randomitega oli abiks https://youtu.be/_SqnzvJuKiA

private int[] punktx = { 35,175,245,350,420,560,630,805,};
//punkti X koordinaatide võimalikud X-teljestiku kohad
private int[] punkty = { 35,140,175,280,385,420,525,665,770,805,840,945,};
//punkti Y koordinaatide võimalikud Y-Teljestiku kohad

private int[] kastx = {280,525,770};
private int[] kasty = {105,455,735};

private int[] kast2x = {210,490,665};
private int[] kast2y = {245,630,910};

private int[] kast3x = {70,385,700};
private int[] kast3y = {350,560,875};

private int[] kast4x = {105,315,595};
private int[] kast4y = {210,490,700};

private int[] kast5x = {140,455,735};
private int[] kast5y = {70,315,595};

//Private int kastx/y kuni kast5x/y on takistuste võimalikud koordinaadid

private Random random = new Random();

private int xpos = random.nextInt(8);
private int ypos = random.nextInt(12);

private int xkast = random.nextInt(3);
private int ykast = random.nextInt(3);

private int x2kast = random.nextInt(3);
private int y2kast = random.nextInt(3);

private int x3kast = random.nextInt(3);
private int y3kast = random.nextInt(3);

private int x4kast = random.nextInt(3);
private int y4kast = random.nextInt(3);

private int x5kast = random.nextInt(3);
private int y5kast = random.nextInt(3);

private int punktid = 0;
private Timer timer;


int R = (int) (Math.random( )*256);
int G = (int)(Math.random( )*256);
int B= (int)(Math.random( )*256);
Color randomColor = new Color(R, G, B); //https://mathbits.com/MathBits/Java/Graphics/Color.htm


int x = 0, y = 35, velX = 0, velY = 0, z = 0, velZ = 0;


Leveliküsija l = new Leveliküsija();


public Raskem(){
	addKeyListener(this);
	setFocusable(true);
	setFocusTraversalKeysEnabled(false);
	timer = new Timer(80,this); //muutes ära numbri Timeri sees saab seada kiiruse
	timer.start();
	
}

public void paint (Graphics g){
	super.paint(g);
	Graphics2D g2 = (Graphics2D) g;
	
	g.setColor(Color.BLACK); //taust
	g.fillRect(0, 0, 1000, 1000);
	
	g.setColor(Color.WHITE); //punktiskoor
	g.setFont(new Font("COMIC SANS", Font.PLAIN, 24));
	g.drawString("punktid: " + punktid, 700, 25);
	
	if (x == 0 && y == 35 && velX == 0 && velY == 0){ //kui mäng algab ja punkt ei liigu, kuvab teksti
	g.setFont(new Font("arial", Font.BOLD, 45));
	g.setColor(Color.WHITE);
	g.drawString("Kogu punaseid täppe, et saada punkte.", 10, 375);
	g.drawString("Liiguta sinist täppi nooleklahvidega.", 40, 455);
	g.drawString("Alustamiseks vajuta nooleklahviga", 50, 535);
	g.drawString("alla või paremale.", 400, 575);
	g.drawString("Ära mine vastu kastidele ja ringidele", 35, 655);
	g.dispose();
	}
	
	g2.setColor(Color.BLUE); //joonsitab punkti
	g2.setStroke(new BasicStroke (4));
	g2.drawOval(x, y, 35, 35);
	
	
	if (x < 0 || x >= 875 || y < 35 || y >= 1015 || x == kastx[xkast] && y == kasty[ykast] ||  x == kast2x[x2kast] && y == kast2y [y2kast]
			||  x == kast3x[x3kast] && y == kast3y [y3kast]
			||  x == kast4x[x4kast] && y == kast4y [y4kast]
			||  x == kast5x[x5kast] && y == kast5y [y5kast]
			||  x == z && y == 385){ // kui punkt läheb piiridest välja või põrkab kokku mingi kastiga, siis kuvab teksti ja peatab aja
		
		g.setColor(Color.WHITE);	
		g.setFont(new Font("arial", Font.BOLD, 50));
		g.drawString("MÄNG LÄBI", 274, 375);
		g.drawString("Sinu punktid: " + punktid, 244, 475);
		g.drawString("Vajuta Tühikut või Nooleklahve,", 60, 575);
		g.drawString("et alustada uuesti", 400, 630);
		timer.stop();
		g.dispose();
					
	}
	    	
	g.setColor(Color.RED); //joonistab täpi mida peab koguma
	g.fillOval(punktx[xpos], punkty[ypos], 35, 35);
	
	
	if (punktx[xpos] == x && punkty[ypos] == y || x < 0 || x > 875 || y < 35 || y > 1050){ 
		//kui sinine punkt "sööb" ära punase täpi, siis lisab juurde ühe punkti ja paneb kastid uude kohta
		punktid++;
		xpos = random.nextInt(8);
		ypos = random.nextInt(12);
		xkast = random.nextInt(3);
		ykast = random.nextInt(3);
		x2kast = random.nextInt(3);
		y2kast = random.nextInt(3);
		x3kast = random.nextInt(3);
		y3kast = random.nextInt(3);
		x4kast = random.nextInt(3);
		y4kast = random.nextInt(3);
		x5kast = random.nextInt(3);
		y5kast = random.nextInt(3);
		
		
		}		
		// joonistatakse kõik kastid/ringid
		g.setColor(Color.RED); 
		g.fillRect(kastx[xkast], kasty[ykast], 35, 35);
		
		g.setColor(Color.YELLOW);
		g.fillRoundRect(kast2x[x2kast], kast2y[y2kast], 35, 35, 35, 35);
		
		
		g2.setColor(Color.GREEN);
		g2.setStroke(new BasicStroke (4)); //muudab tühja ringi paksust (http://stackoverflow.com/questions/2839508/java2d-increase-the-line-width)
		g2.drawOval(kast3x[x3kast], kast3y[y3kast], 35, 35);
		
		g2.setColor(Color.BLUE);
		g2.drawRect(kast4x[x4kast], kast4y[y4kast], 35, 35);
		
		g2.setColor(Color.WHITE);
		g2.drawRect(kast5x[x5kast], kast5y[y5kast],35, 35);
		
		g2.setColor(randomColor);
		g.drawRect(z, 385, 35,35);
		
		if (z == 1050) //kui liikuv kast läheb piiridest välja, pannakse see tagasi 0 punkti
		{
			z = 0;
		}
				
		}	

	@Override
	public void actionPerformed(ActionEvent e) 
	{	
		{
		x = x + velX;
		y = y + velY;
		z = z + velZ;
		repaint();
		}
		{
		if  (x < 0 || x >= 875 || y < 35 || y >= 1015 || x == kastx[xkast] && y == kasty[ykast] 
				||  x == kast2x[x2kast] && y == kast2y [y2kast]
				||  x == kast3x[x3kast] && y == kast3y [y3kast]
				||  x == kast4x[x4kast] && y == kast4y [y4kast]
				||  x == kast5x[x5kast] && y == kast5y [y5kast]
				||  x == z && y == 385){
				//kui punkt põrkab kokku mingi kastiga või läheb piiridest välja, peatab aja ja liikumise
				velX = 0;
				velY = 0;						 
				timer.stop();
						
			}
		}
		
		
	}
	@Override
	public void keyPressed(KeyEvent e) { //abiks oli https://youtu.be/oynZhQjMv0c
		 
		int c = e.getKeyCode();
		
		if  (x < 0 || x >= 875 || y < 35 || y >= 1015 || x == kastx[xkast] && y == kasty[ykast] 
				||  x == kast2x[x2kast] && y == kast2y [y2kast]
				||  x == kast3x[x3kast] && y == kast3y [y3kast]
				||  x == kast4x[x4kast] && y == kast4y [y4kast]
				||  x == kast5x[x5kast] && y == kast5y [y5kast]
				||  x == z && y == 385		
				&& c == KeyEvent.VK_SPACE)
			//kui punkt on juba piiridest välja läinud või kastiga kokku põrganud ning aeg on peatatud, paneb punkti algkooridaatidele
			//, nullib ära skoori ja taaskäivitab aja
		{
			
			velX = 0;
			velY = 0;
			velZ = 0;
			punktid = 0;
			x = 0;
			y = 35;
			timer.restart();
		}
		
		
		//kui kasutaja vajutab teatud nuppudele siis liigub punkt teatud suunas
		if (c == KeyEvent.VK_RIGHT)
			
		{		
			velX = +35;
			velY = 0;
			velZ = +35;		
		}
		if (c == KeyEvent.VK_LEFT)
		{
			velX = -35;
			velY = 0;
			velZ = +35;
		}
		
		if (c == KeyEvent.VK_DOWN)
		{
			velX = 0;
			velY = +35;
			velZ = +35;
		}
		if (c == KeyEvent.VK_UP)
		{
			velX = 0;
			velY = -35;
			velZ = +35;
		}
		
		//kui kasutaja vajutab nuppu "esc" paneb kinni kõik aknad.
		if (c == KeyEvent.VK_ESCAPE)
		{
			System.exit(0);
		}
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
