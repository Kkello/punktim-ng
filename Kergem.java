package punktimäng;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Kergem extends JPanel implements ActionListener, KeyListener{
	
	private int[] punktx = { 35,140,175,245,350,420,455,560,630,735,805,105, 315, 595};
	//punkti X koordinaatide võimalikud X-teljestiku kohad
	private int[] punkty = { 35,70,140,175,280,315,385,420,525,595,665,770,805,840,945,210, 490,700};
	//punkti Y koordinaatide võimalikud Y-Teljestiku kohad
	
	private int[] kastx = {280, 525,770};
	private int[] kasty = {105, 455, 735 };
	
	private int[] kast2x = {210,490,665};
	private int[] kast2y = { 245, 630, 910 };
	
	private int[] kast3x = {70,385,700};
	private int[] kast3y = { 350, 560, 875};
	
	private Random random = new Random();
	
	
	private int xpos = random.nextInt(14);
	private int ypos = random.nextInt(18);
	
	private int xkast = random.nextInt(3);
	private int ykast = random.nextInt(3);

	private int x2kast = random.nextInt(3);
	private int y2kast = random.nextInt(3);
	
	private int x3kast = random.nextInt(3);
	private int y3kast = random.nextInt(3);
	
	
	private int punktid = 0;
	private Timer timer;
	
	int x = 0, y = 35, velX = 0, velY = 0;
	
	
	Leveliküsija l = new Leveliküsija();
	
	
	public Kergem(){
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(110,this);
		timer.start();
		
	}
	
	public void paint (Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		
		
		g.setFont(new Font("COMIC SANS", Font.PLAIN, 24));
		g.drawString("punktid: " + punktid, 700, 25);
		
		g.setFont(new Font("arial", Font.BOLD, 45));
		if (x == 0 && y == 35 && velX == 0 && velY == 0){
		g.drawString("Kogu punaseid täppe, et saada punkte.", 10, 375);
		g.drawString("Liiguta sinist täppi nooleklahvidega.", 40, 455);
		g.drawString("Alustamiseks vajuta nooleklahviga", 50, 535);
		g.drawString("alla või paremale.", 400, 575);
		g.drawString("Ära mine vastu kastidele ja ringidele", 35, 655);
		g.dispose();
		}
		
		g2.setColor(Color.BLUE);
		g2.setStroke(new BasicStroke (4));
		g2.drawOval(x, y, 35, 35);
		
		//ImageIcon playerI = new ImageIcon("uss.png");
		//player = playerI.getImage();	
		//g.drawImage(player,x, y, this);
		
		
		if (x < 0 || x > 875 || y < 35 || y > 1050 || x == kastx[xkast] && y == kasty[ykast] ||  x == kast2x[x2kast] && y == kast2y [y2kast]
				||  x == kast3x[x3kast] && y == kast3y [y3kast]){
			g.setColor(Color.BLACK);
			g.setFont(new Font("arial", Font.BOLD, 50));
			g.drawString("MÄNG LÄBI", 274, 375);
			g.drawString("Sinu punktid: " + punktid, 244, 475);
			g.drawString("Vajuta Tühikut või Nooleklahve,", 60, 575);
			g.drawString("et alustada uuesti", 400, 630);
			timer.stop();
			g.dispose();
						
		}
		    	
		g.setColor(Color.RED);
		g.fillOval(punktx[xpos], punkty[ypos], 35, 35);
		
		
		//ImageIcon punktI = new ImageIcon("punkt.png");
		//punkt = punktI.getImage();
		//g.drawImage(punkt,punktx[xpos], punkty[ypos], this);
	
		
		if (punktx[xpos] == x && punkty[ypos] == y || x < 0 || x > 875 || y < 35 || y > 1050){
			punktid++;
			xpos = random.nextInt(14);
			ypos = random.nextInt(18);
			xkast = random.nextInt(3);
			ykast = random.nextInt(3);
			x2kast = random.nextInt(3);
			y2kast = random.nextInt(3);
			x3kast = random.nextInt(3);
			y3kast = random.nextInt(3);
			
			
			}		
			g.setColor(Color.RED);
			g.fillRect(kastx[xkast], kasty[ykast], 35, 35);
			
			g.setColor(Color.YELLOW);
			g.fillRoundRect(kast2x[x2kast], kast2y[y2kast], 35, 35, 35, 35);
			
			
			g2.setColor(Color.GREEN);
			g2.setStroke(new BasicStroke (4));
			g2.drawOval(kast3x[x3kast], kast3y[y3kast], 35, 35);
					
			
			}	
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{	
		{
		x = x + velX;
		y = y + velY;
		repaint();
		}
		{
			if  (x < 0 || x > 875 || y < 35 || y > 1050 || x == kastx[xkast] && y == kasty[ykast] 
					||  x == kast2x[x2kast] && y == kast2y [y2kast]
					||  x == kast3x[x3kast] && y == kast3y [y3kast]){
				velX = 0;
				velY = 0;						 
				timer.stop();
				
			
			}
		}
		
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		 
		int c = e.getKeyCode();
		
		if  (x < 0 || x > 875 || y < 35 || y > 1050 || x == kastx[xkast] && y == kasty[ykast] 
				||  x == kast2x[x2kast] && y == kast2y [y2kast]
				||  x == kast3x[x3kast] && y == kast3y [y3kast]
				&& c == KeyEvent.VK_SPACE)
		{
			
			velX = 0;
			velY = 0;
			punktid = 0;
			x = 0;
			y = 35;
			timer.restart();
		}
			
		if (c == KeyEvent.VK_RIGHT)
		{
			
			velX = +35;
			velY = 0;
			
		}
		if (c == KeyEvent.VK_LEFT )
		{
			velX = -35;
			velY = 0;
		}
		if (c == KeyEvent.VK_DOWN )
		{
			velX = 0;
			velY = +35;
		}
		if (c == KeyEvent.VK_UP )
		{
			velX = 0;
			velY = -35;
		}
		
		if (c == KeyEvent.VK_ESCAPE)
		{
			System.exit(0);
		}
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
// abiks oli https://youtu.be/_SqnzvJuKiA
