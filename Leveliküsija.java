package punktimäng;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Leveliküsija extends JFrame implements ActionListener, KeyListener {
	public static void main(String[] args) {
		new Leveliküsija();
	}
	private JLabel raskusaste;
	private JButton kerge;
	private JButton raske;
	
	
	public Leveliküsija(){
		
		setLayout(null);
		setTitle("VALI RASKUSASTE");//tekst mida kuvatakse üleval ( ei tea selle nime)
		setSize(820, 300);//akna suurus
		setLocationRelativeTo(null);//et avades asuks aken ekraani keskel
		setResizable(false);//et kasutaja ei saaks akna suurust muuta
		setVisible(true);//et oleks kõike näha
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //suleks kõik aknad kui kasutaja vajutab x
		this.addKeyListener(this); //lisab keylisteneri
		
		//tekst nuppude kohal
		raskusaste = new JLabel("Vali raskusaste:");
		raskusaste.setFont(new Font("Arial", Font.PLAIN, 40));
		raskusaste.setBounds(100, 10, 1000, 100);
		add(raskusaste);
		
		//nupp kerge
		kerge = new JButton("Kerge");
		kerge.setFont(new Font("Arial", Font.PLAIN, 40));
		kerge.setBounds(90, 100, 300, 100);
		kerge.addActionListener(this);
		add(kerge);
		
		//nupp raske
		raske = new JButton("Raske");
		raske.setFont(new Font("Arial", Font.PLAIN, 40));
		raske.setBounds(420, 100,300,100);
		raske.addActionListener(this);
		add(raske);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == kerge){
			WindowKerge w = new WindowKerge();
					
		}
			//Game g = new Game();
		if (arg0.getSource() == raske){
			WindowRaskem wr = new WindowRaskem();
			
		}			
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		int c = arg0.getKeyCode();
		
		if (c == KeyEvent.VK_ESCAPE) //kui kasutaja vajutab nuppu "esc", suleb akna
		{
			System.exit(0);
		}
	
	}






	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}






	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

	}

// abiks oli: https://youtu.be/2KQ2ryPS4Kg