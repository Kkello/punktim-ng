package punktimäng;

import javax.swing.JFrame;

public class WindowRaskem {
	public static void main(String[] args) {
		new WindowRaskem();
	}
	
	public WindowRaskem(){
		Raskem r = new Raskem();
		JFrame f = new JFrame();
		f.setTitle("Punktimäng");
		f.setSize(875, 1050);//mänguakna suurus
		f.setLocationRelativeTo(null);//et peale nupulevajutust tuleks see aken keskele
		f.setResizable(false);//et kasutaja ei saaks suurust muuta
		f.setVisible(true);//et me kõike näeks
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);// kui kasutaja vajutab x siis sulgub ainult mänguaken, leveliküsija jääb avatuks
		f.add(r);
		
	}
	
}
//Abiks oli https://www.youtube.com/watch?v=dEKs-3GhVKQ&list=PLah6faXAgguMnTBs3JnEJY0shAc18XYQZ